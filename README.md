## Wabi Lab Challenge

Welcome to the Wabi Challenge repository

### Requirements
This project needs those components to be installed:

* docker
* docker-compose

###Purpose of project
Replicate 3 pages from the official Wabi Lab website [visible here @wabi.it](https://www.google.com)

Pages that needs to be replicated:

* Homepage
* Projects list
* Project detail

A small CMS needs to be created for the projects management with users authentication.

### Project scaffolding
* data: database main folder
* laradock: main docker folder
* wabi: main application folder

### Running the docker

To startup the project just run the `start.sh` bash file. It will run the creation of the docker images (if it's the first time, just take a coffee and wait). Be sure to have the port 80 available and not busy from other processes (like Vagrant, XAMP, WAMP, MAMP or Apache/Nginx)

For informations about the docker, just visit the [Laradock website](https://laradock.io)

### Accessing the project
Visit [http://localhost](http://localhost) to take a look at the project.

The CMS login is provided by the link `login` visibile near the languages menù voices. To access the CMS please use the following credentials:

* username: admin@wabichallenge.com
* password: wabichallenge


