<nav class="navbar navbar-expand-lg navbar-light justify-content-between fixed-top white">
    <a class="navbar-brand" href="#" style="width: 91px;">
        <svg version="1.1" id="Livello_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
             x="0px" y="0px" viewBox="0 0 364 100" style="enable-background:new 0 0 364 100;" xml:space="preserve"
             class="injected-svg svg-injector logo-desktop">
            <g>
                <path class="p-1"
                      d="M108.2,100H82.1L68.4,31.1h-0.3L54.4,100H28L0.9,0H27l14.1,73.1h0.3L56.1,0h24.5l14.7,73.1h0.3L109.7,0h26L108.2,100z"></path>
                <path class="p-1"
                      d="M166.8,0h26.5l37.4,100h-27.2l-6.2-17.9h-34.9l-6.3,17.9h-26.8L166.8,0z M168.5,62.8h22.7l-11.1-35h-0.3L168.5,62.8z"></path>
                <path class="p-1"
                      d="M238.3,0H285c17.2-0.3,36.1,4.2,36.1,25.1c0,9-5.3,16.3-13.2,20.2c10.6,3.1,17.2,12.3,17.2,23.7   c0,23.8-17.5,31.1-38.9,31.1h-48V0z M264.3,39.4h20.2c4.5,0,10.6-2.4,10.6-9.5c0-7.3-5.2-9.7-10.6-9.7h-20.2V39.4z M264.3,79.1   h20.9c8.3,0,14-2.9,14-11.2c0-8.8-6.2-11.8-14-11.8h-20.9V79.1z"></path>
                <path class="p-1" d="M363.1,18.2h-23.8V0h23.8V18.2z M339.3,27.3h23.8V100h-23.8V27.3z"></path>
            </g>
        </svg>
    </a>
    <ul class="navbar-nav align-items-center">
        <li class="nav-item">
            <a class="nav-link" href="#">it</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">en</a>
        </li>
        @if (Route::has('login'))
            <li class="nav-item">
                @auth
                    <a class="nav-link" href="{{ route('home') }}">cms</a>
                @else
                    <a class="nav-link" href="{{ route('login') }}">login</a>
                @endauth
            </li>
        @endif
        <li class="nav-item">
            <button class="btn nav-link ml-lg-2 mr-2" id="menuToggler" type="button">
                <i class="fa fa-bars"></i>
            </button>
        </li>
    </ul>
</nav>
