@extends('layout.main')


@section('content')
    <div class="container-fluid h-100">
        <div class="row align-items-center text-center h-100 justify-content-center" id="homeVideo">
            <video playsinline="" muted="" loop="" poster="{{asset('/img/home/video-background.jpg') }}"
                   style="background-image:url({{asset('/img/home/video-background.jpg') }});" class="video-bg"
                   autoplay>
                <source src="{{asset('/video/sample.mp4') }}" type="video/mp4">
            </video>
            <div id="rollingLogo">
                <img id="out" src="{{asset('/img/logo-wabi-out.svg') }}" alt="">
                <img id="in" src="{{asset('/img/logo-wabi-in.svg') }}" alt="">
            </div>
        </div>
    </div>
@endsection
