<?php

use Faker\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('it_IT');
        // $this->call(UsersTableSeeder::class);

        try {
            DB::table('users')->truncate();
            DB::table('users')->insert(
                [
                    'name' => 'Wabi Admin',
                    'email' => 'admin@wabichallenge.com',
                    'password' => bcrypt('wabichallenge')
                ]
            );


            DB::table('customers')->truncate();
            for ($i = 0; $i < 10; $i++) {
                DB::table('customers')->insert([
                    'name' => $faker->company,
                    'member_since' => $faker->year()
                ]);
            }

            DB::table('projects')->truncate();
            for ($i = 0; $i < 20; $i++) {
                DB::table('projects')->insert([
                    'title' => $faker->realText(20),
                    'description' => $faker->randomHtml(2, 4),
                    'preview_image' => $faker->imageUrl(640, 480, 'technics'),
                    'url' => $faker->url,
                    'customer_id' => $faker->randomNumber(1, true)
                ]);
            }
        } catch (QueryException $e) {
            dd($e->getMessage());
        }
    }
}
