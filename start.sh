#!/usr/bin/env bash
cd laradock
docker-compose up -d nginx php-fpm workspace
docker ps
echo "Updating dependencies (composer and npm)"
docker-compose exec workspace bash -c "cd wabi && composer install && npm install"
echo "Entering docker workspace bash"
docker-compose exec workspace bash
